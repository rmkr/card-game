# Card Game

This application builds two separate parts. 
1. A `libcard.a` library. 
2. Card Tests `<build_dir/tests/card_tests`

### Pre-Requirements

* CMake >=3.10: Installed on system.
* g++

### To Build

```bash
mkdir build & cd build
cmake ..
make
```

### To Run Tests
```bash
# Build the code
cd build/tests
./card_tests
```
