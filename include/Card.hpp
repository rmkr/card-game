#ifndef _CARDS_CARD_HPP
#define _CARDS_CARD_HPP

#include <iostream>
#include <string>

namespace CardGame
{
    /**
     * The Card Object is used to represent a single card in a standard playing
     * card deck.
     */
    class Card
    {
    public:
        /**
         * This is the basic constructor for creating a card suit.
         * @param face The string representing the face of the card.
         * @param suit The string representing the card suit.
         */
        Card(const std::string &face, const std::string &suit);

        /**
         * Override the << operation, this will call us to pass the card
         * directly to stream.
         */
        friend std::ostream &operator<<(std::ostream &os, const Card &card);

        /**
         * Add a operation to check if two cards are the same.
         * @param rhs The card being compared against
         * @return True: If the Cards are equal.
         */
        bool operator==(const Card &rhs) const;

        // =========================================================================
        // GETTERS/SETTERS
        // =========================================================================
        [[nodiscard]] const std::string &get_face() const;
        void set_face(const std::string &face);
        [[nodiscard]] const std::string &get_suit() const;
        void set_suit(const std::string &suit);

    protected:
        std::string _suit;
        std::string _face;
    };
}// namespace CardGame

#endif//_CARDS_CARD_HPP
