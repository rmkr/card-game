#ifndef _CARDS_DECKOFCARDS_HPP
#define _CARDS_DECKOFCARDS_HPP

#include <Card.hpp>
#include <vector>

// The total number of cards in our deck.

namespace CardGame
{

    class DeckOfCards
    {
    public:
        /**
         * Creates a deck the size of length of card_faces x length of
         * card_suits.
         *
         * @param card_faces A vector containing all the face values of the card
         * deck.
         *
         * @param card_suits A vector containing all the available suits in the
         * deck.
         */
        DeckOfCards(const std::vector<std::string> &card_faces,
                    const std::vector<std::string> &card_suits);

        /**
         * This function shuffles the deck and places all cards back into the
         * deck.
         */
        void shuffle();

        /**
         * This function deals one card at a time as long as a card is still in
         * the deck.
         *
         * @return The card dealt.
         */
        Card deal_card();

    private:
        /// Stores the Deck.
        std::vector<Card> deck;

        /// Tracks the current Card
        unsigned int current_card;
    };

}// namespace CardGame

#endif//_CARDS_DECKOFCARDS_HPP
