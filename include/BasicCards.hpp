#ifndef _CARDS_BASICCARDS_HPP
#define _CARDS_BASICCARDS_HPP

#include <string>
#include <vector>

const std::vector<std::string> FACES = {
        "Ace",   "Two",  "Three", "Four", "Five",  "Six", "Seven",
        "Eight", "Nine", "Ten",   "Jack", "Queen", "King"};

const std::vector<std::string> SUITS = {"Hearts", "Clubs", "Spades",
                                        "Diamonds"};

#endif//_CARDS_BASICCARDS_HPP
