#include <BasicCards.hpp>
#include <DeckOfCards.hpp>
#include <catch_amalgamated.hpp>

using namespace CardGame;

TEST_CASE("Deal Cards", "[!throws]")
{
    DeckOfCards deck_of_cards = DeckOfCards(FACES, SUITS);

    SECTION("Deal the max number of cards")
    {
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            REQUIRE_NOTHROW(deck_of_cards.deal_card());
        }

        REQUIRE_THROWS(deck_of_cards.deal_card());
    }// The Code Resets to the code at the begining of the test case

    SECTION("Shuffle Cards")
    {
        std::vector<Card> unshuffled;
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            REQUIRE_NOTHROW(unshuffled.push_back(deck_of_cards.deal_card()));
        }

        // Shuffle The Deck
        deck_of_cards.shuffle();

        // Get Shuffled Deck
        std::vector<Card> shuffled;
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            REQUIRE_NOTHROW(shuffled.push_back(deck_of_cards.deal_card()));
        }

        // Verify that the vectors are exactly the same.
        int diff_count = 0;
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            if (!(shuffled[i] == unshuffled[i])) diff_count += 1;
        }

        REQUIRE(diff_count >= 1);
    }

    SECTION("Print Cards Ordered")
    {
        std::cout << "Ordered Cards: \n";
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            std::cout << "Card " << deck_of_cards.deal_card() << std::endl;
        }
    }

    SECTION("Print Cards Unorderd")
    {
        deck_of_cards.shuffle();
        std::cout << "\nUnordered Cards: \n";
        for (unsigned int i = 0; i < FACES.size() * SUITS.size(); i++)
        {
            std::cout << "Card " << deck_of_cards.deal_card() << std::endl;
        }
    }
}
