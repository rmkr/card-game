#include <Card.hpp>
#include <string>

namespace CardGame
{

    Card::Card(const std::string &face, const std::string &suit)
    {
        _face = face;
        _suit = suit;
    }

    std::ostream &operator<<(std::ostream &os, const Card &card)
    {
        os << card.get_face() << " of " << card.get_suit();
        return os;
    }

    bool Card::operator==(const Card &rhs) const
    {
        if (this->_face == rhs._face && this->_suit == rhs._suit) return true;
        return false;
    }

    // =============================================================================
    // GETTERS/SETTERS
    // =============================================================================
    const std::string &Card::get_face() const { return _face; }

    void Card::set_face(const std::string &face) { _face = face; }

    const std::string &Card::get_suit() const { return _suit; }

    void Card::set_suit(const std::string &suit) { _suit = suit; }
}// namespace CardGame
