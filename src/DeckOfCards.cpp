#include <DeckOfCards.hpp>
#include <cstdlib>
#include <ctime>
#include <stdexcept>

namespace CardGame
{
    DeckOfCards::DeckOfCards(const std::vector<std::string> &card_faces,
                             const std::vector<std::string> &card_suits)
    {
        for (const auto &face : card_faces)
        {
            for (const auto &suit : card_suits)
            {
                deck.emplace_back(face, suit);
            }
        }

        current_card = 0;
    }

    void DeckOfCards::shuffle()
    {
        current_card = 0;
        for (auto &first : deck)
        {
            // Get a Card to swap with.
            int second = (rand() + time(nullptr)) % deck.size();

            // Swap the Cards
            Card temp    = first;
            first        = deck[second];
            deck[second] = temp;
        }
    }

    Card DeckOfCards::deal_card()
    {
        if (current_card == deck.size())
            throw std::runtime_error("Empty Deck Please Shuffle");

        return deck[current_card++];
    }
}// namespace CardGame
