cmake_minimum_required(VERSION 3.10)
project(cards)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-W -Wall -Wextra)

# Add Static Libary
add_library(cards STATIC)

# Location of Headers
target_include_directories(cards PRIVATE include)

# Add Source Files
add_subdirectory(src)

# Add tests
add_subdirectory(tests)
